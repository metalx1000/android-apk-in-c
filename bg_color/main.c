//Original Code Copyright (c) 2011-2020 <>< Charles Lohr - Under the MIT/x11 or NewBSD License you choose.
//Final code by Kris Occhipinti 2020
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "os_generic.h"
#include <GLES3/gl3.h>
#include <asset_manager.h>
#include <asset_manager_jni.h>
#include <android_native_app_glue.h>
#include <android/sensor.h>
#include "CNFGAndroid.h"

#define CNFG_IMPLEMENTATION
#define CNFG3D
#include "CNFG.h"
ASensorManager * sm;
const ASensor * as;
ASensorEventQueue* aeq;
ALooper * l;


void SetupIMU()
{
  sm = ASensorManager_getInstance();
  as = ASensorManager_getDefaultSensor( sm, ASENSOR_TYPE_GYROSCOPE );
  l = ALooper_prepare( ALOOPER_PREPARE_ALLOW_NON_CALLBACKS );
  aeq = ASensorManager_createEventQueue( sm, (ALooper*)&l, 0, 0, 0 ); //XXX??!?! This looks wrong.
  ASensorEventQueue_enableSensor( aeq, as);
  printf( "setEvent Rate: %d\n", ASensorEventQueue_setEventRate( aeq, as, 10000 ) );
}

unsigned frames = 0;
unsigned long iframeno = 0;

void AndroidDisplayKeyboard(int pShow);

int lastbuttonx = 0;
int lastbuttony = 0;
int lastmotionx = 0;
int lastmotiony = 0;
int lastbid = 0;
int lastmask = 0;
int lastkey, lastkeydown;


static int keyboard_up;

void HandleKey( int keycode, int bDown )
{
  lastkey = keycode;
  lastkeydown = bDown;
  if( keycode == 10 && !bDown ) { keyboard_up = 0; AndroidDisplayKeyboard( keyboard_up );  }

  if( keycode == 4 ) { AndroidSendToBack( 1 ); } //Handle Physical Back Button.

}

void HandleButton( int x, int y, int button, int bDown )
{
  lastbid = button;
  lastbuttonx = x;
  lastbuttony = y;

}

void HandleMotion( int x, int y, int mask )
{
  lastmask = mask;
  lastmotionx = x;
  lastmotiony = y;
}

#define HMX 162
#define HMY 162
short screenx, screeny;
float Heightmap[HMX*HMY];

extern struct android_app * gapp;


void HandleDestroy()
{
  printf( "Destroying\n" );
  exit(10);
}

volatile int suspended;

void HandleSuspend()
{
  suspended = 1;
}

void HandleResume()
{
  suspended = 0;
}

uint32_t randomtexturedata[256*256];

int msg(int  color){
    CNFGColor( 0xffffff );
    CNFGPenX = 0; CNFGPenY = 600;
    char msg[50];
    sprintf( msg, "%d", color);
    CNFGDrawText( msg, 20 );
    glLineWidth( 2.0 );

    return 0;
}

int main()
{
  CNFGSetupFullscreen( "Test Bench", 0 );

  SetupIMU();
  int color = 0;
  while(1)
  {
    iframeno++;
    color+=100;
    CNFGBGColor = color;

    CNFGHandleInput();

    if( suspended ) { usleep(50000); continue; }
    CNFGClearFrame();

    msg(color);

    CNFGFlushRender();
    CNFGSwapBuffers();

  }
  return(0);
}

